import time
import random
from adafruit_circuitplayground.express import cpx

MAX_LEVELS = 10
LEVEL_TIMER = 0.250

LEVEL_COLORS = [0x00FF00, 0x00FF04, 0x3BFF00, 0xEEFF00, 0xFFD800, 0xFFAE00, 0xFF8300, 0xFF6100, 0xFF3200, 0xFF0000]
HITS_PER_LEVEL = {
    1: 2,
    2: 8,
    3: 12,
    4: 15,
    5: 20,
    6: 27,
    7: 35,
    8: 48,
    9: 60,
    10: 80
}

sounds = [0, 0, 0, 0]

def select_level():
    level = 1
    cpx.pixels.fill(0)
    cpx.pixels[0] = 0x00FF00


    while not cpx.button_b:
        if cpx.button_a:
            level = (level + 1) % (MAX_LEVELS + 1)

            if level == 0:
                level = 1

            cpx.pixels.fill(0)

            for pixel in range(level):
                cpx.pixels[pixel] = LEVEL_COLORS[level - 1]
            time.sleep(LEVEL_TIMER)
    
    return level
            
def generate_sequence(level):
    seq = []

    for i in range(HITS_PER_LEVEL[level]):
        side = random.randint(1, 4)
        seq.append(side)

    return seq

def turn_off_lights():
    cpx.pixels.fill(0)

def show_sequence(sequence, step):
    for i in range(0, step):
        turn_off_lights()
        
        print(seq[i])
        if seq[i] == 1:
            cpx.pixels[0] = 0x0400FF
            cpx.pixels[1] = 0x0400FF
            cpx.pixels[2] = 0x0400FF
        elif seq[i] == 2:
            cpx.pixels[2] = 0x00FF0C
            cpx.pixels[3] = 0x00FF0C
            cpx.pixels[4] = 0x00FF0C
        elif seq[i] == 3:
            cpx.pixels[5] = 0xEEFF00
            cpx.pixels[6] = 0xEEFF00
            cpx.pixels[7] = 0xEEFF00
        else:
            cpx.pixels[7] = 0xFF00D0
            cpx.pixels[8] = 0xFF00D0
            cpx.pixels[9] = 0xFF00D0

        time.sleep(1)
    turn_off_lights()

def check_input(user_array, seq_array):
    for i in range(0, len(user_array)):
        if (seq_array[i] != user_array[i]):
            return False

    return True

def show_lights(n):
    if n == 1:
        cpx.pixels[0] = 0x0400FF
        cpx.pixels[1] = 0x0400FF
        cpx.pixels[2] = 0x0400FF
    elif n == 2:
        cpx.pixels[2] = 0x00FF0C
        cpx.pixels[3] = 0x00FF0C
        cpx.pixels[4] = 0x00FF0C
    elif n == 3:
        cpx.pixels[5] = 0xEEFF00
        cpx.pixels[6] = 0xEEFF00
        cpx.pixels[7] = 0xEEFF00
    else:
        cpx.pixels[7] = 0xFF00D0
        cpx.pixels[8] = 0xFF00D0
        cpx.pixels[9] = 0xFF00D0

    time.sleep(0.4)
    turn_off_lights()


def get_user_input(seq, step):
    user_input = []
    input_touched = False

    while True:
        input_touched = False

        if cpx.touch_A1:
            user_input.append(3)
            input_touched = True
        if cpx.touch_A2:
            user_input.append(4)
            input_touched = True
        if cpx.touch_A3:
            user_input.append(4)
            input_touched = True
        if cpx.touch_A4:
            user_input.append(1)
            input_touched = True
        if cpx.touch_A5:
            user_input.append(1)
            input_touched = True
        if cpx.touch_A6:
            user_input.append(2)
            input_touched = True
        if cpx.touch_A7:
            user_input.append(2)
            input_touched = True
        
        if input_touched:
            show_lights(user_input[len(user_input) - 1])
            result = check_input(user_input, seq)

            if result:
                print("Bon résultat")
                cpx.play_tone(0, 0.5)
                if len(user_input) == step:
                    print("Gagné")
                    return True
            else:
                print("Perdu")
                return False

        time.sleep(0.25)

def lost_game_effect():
    c = 0

    while True:
        turn_off_lights()

        cpx.pixels.fill((255, 0, 0))

        time.sleep(0.25)
        c = c + 1

        if c == 5:
            return

def win_game_effect():
    for i in range(2):
        turn_off_lights()

        for i in range(len(cpx.pixels)):
            cpx.pixels[i] = 0x00FF00
            time.sleep(0.25)

def generate_sounds():
    pass

step = 1
cpx.pixels.brightness = 0.03

while True:
    selected_level = select_level()
    step = 1
    turn_off_lights()

    print("Niveau sélectionné " + str(selected_level))

    lost_game = False
    seq = generate_sequence(selected_level)
    
    print(seq)

    while not lost_game:
        show_sequence(seq, step)
        result = get_user_input(seq, step)
    
        if result:
            step = step + 1
            
            if step > len(seq):
                win_game_effect()
                break
        else:
            lost_game = True
            cpx.play_tone(150, 1)
            lost_game_effect()

    time.sleep(0.250)