# **Projet Python embarqué**

## Simon's game

### Règles du jeu

Le jeu du simon est un jeu de mémorisation de couleurs. Le but est de reproduire un pattern de couleurs. Il est possible de choisir un niveau de difficulté. Plus la difficulté est grande, plus le nombre de couleurs à retenir est grand.

Voici chaque niveau de difficulté et le nombre de couleurs à retenir :

- Niveau 1 : 2 couleurs
- Niveau 2 : 8 couleurs
- Niveau 3 : 12 couleurs
- Niveau 4 : 15 couleurs
- Niveau 5 : 20 couleurs
- Niveau 6 : 27 couleurs
- Niveau 7 : 35 couleurs
- Niveau 8 : 48 couleurs
- Niveau 9 : 60 couleurs
- Niveau 10 : 80 couleurs

### Logique du jeu

La logique du jeu est simple : Le joueur choisi le niveau de difficulté, il peut la modifier en appuyant sur le bouton A et appuie sur le bouton B pour commencer.

Ensuite la partie se lance, et le pattern à reproduire commence. Le joueur doit ensuite toucher les capteurs dans l'ordre dans laquelle les couleurs sont apparues. 

Si la partie est gagnée, toutes les diodes s'allument en vert. Si la partie est perdu, toutes les diodes s'allument en rouge. 


### Difficultés rencontrées

- La logique du jeu a codé en python
- Difficulté la logique évenementielle + la récupération des valeurs de chaque capteur.

